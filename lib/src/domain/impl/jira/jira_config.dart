import 'package:flutter/widgets.dart';

class JiraConfig {
  final String jiraName;
  final String username;
  final String apiToken;
  final String projectKey;
  final String issueType;

  JiraConfig(
      {@required this.jiraName,
      @required this.username,
      @required this.apiToken,
      @required this.projectKey,
      @required this.issueType});
}
