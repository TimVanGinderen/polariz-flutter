import 'dart:io';
import 'dart:typed_data';

import 'package:path_provider/path_provider.dart';
import 'package:polariz/src/domain/feedback_handler.dart';
import 'package:polariz/src/domain/impl/jira/jira_client.dart';
import 'package:polariz/src/domain/impl/jira/jira_config.dart';

class JiraFeedbackHandler extends FeedbackHandler {
  final JiraConfig jiraConfig;

  JiraClient jiraClient;

  JiraFeedbackHandler(this.jiraConfig) {
    jiraClient = JiraClient(
        getJiraApiDio(jiraConfig.username, jiraConfig.apiToken),
        baseUrl: "https://${jiraConfig.jiraName}.atlassian.net");
  }

  @override
  Future<bool> handleFeedback(String userId, String message,
      Uint8List feedbackScreenshot, List<File> attachments) async {
    try {
      final CreateIssueResponse response = await jiraClient.createIssue(
        CreateIssue(
          fields: Fields(
            project: Project(key: jiraConfig.projectKey),
            description: message,
            summary: "Feedback from user ${userId ?? ""}",
            issuetype: IssueType(name: jiraConfig.issueType),
          ),
        ),
      );
      await postAttachment(response.key, "screenshot.png", feedbackScreenshot);
      await Future.forEach(attachments, (file) async {
        await postFile(response.key, file);
      });
      return true;
    } on Exception {
      return false;
    } on Error {
      return false;
    }
  }

  Future<void> postAttachment(
      String issueId, String fileName, Uint8List content) async {
    String tempDir = (await getTemporaryDirectory()).path;
    var tempFile = File("$tempDir/$fileName");
    await tempFile.writeAsBytes(content);
    await jiraClient.postAttachment(issueId, tempFile);
    await tempFile.delete();
  }

  Future<void> postFile(String issueId, File file) async {
    await jiraClient.postAttachment(issueId, file);
  }
}
