import 'dart:convert';
import 'dart:io';

import 'package:json_annotation/json_annotation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'jira_client.g.dart';

Dio getJiraApiDio(String username, String apiToken) {
  final Dio dio = Dio();
  dio.interceptors.add(PrettyDioLogger(logPrint: (message) {
    print(message.toString());
  }));
  dio.options.headers.remove("User-Agent");
  dio.options.headers["X-Atlassian-Token"] = "nocheck";
  dio.options.headers["Content-Type"] = "application/json";
  dio.options.followRedirects = false;
  dio.options.validateStatus = (status) {
    return status < 500;
  };
  dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions options) =>
          requestInterceptor(options, username, apiToken)));
  return dio;
}

dynamic requestInterceptor(
    RequestOptions options, String username, String apiToken) async {
  final String basicValue = base64.encode(utf8.encode("$username:$apiToken"));
  options.headers.addAll({"Authorization": "Basic $basicValue"});
  return options;
}

@RestApi()
abstract class JiraClient {
  factory JiraClient(Dio dio, {String baseUrl}) = _JiraClient;

  @POST("/rest/api/2/issue")
  Future<CreateIssueResponse> createIssue(@Body() CreateIssue body);

  @POST("/rest/api/2/issue/{key}/attachments")
  Future<void> postAttachment(@Path() String key, @Part() File file);
}

@JsonSerializable()
class CreateIssue {
  Fields fields;

  CreateIssue({this.fields});

  factory CreateIssue.fromJson(Map<String, dynamic> json) =>
      _$CreateIssueFromJson(json);
  Map<String, dynamic> toJson() => _$CreateIssueToJson(this);
}

@JsonSerializable()
class Fields {
  String summary;
  String description;
  Project project;
  IssueType issuetype;

  Fields({this.summary, this.description, this.project, this.issuetype});

  factory Fields.fromJson(Map<String, dynamic> json) => _$FieldsFromJson(json);
  Map<String, dynamic> toJson() => _$FieldsToJson(this);
}

@JsonSerializable()
class Project {
  String key;

  Project({this.key});

  factory Project.fromJson(Map<String, dynamic> json) =>
      _$ProjectFromJson(json);
  Map<String, dynamic> toJson() => _$ProjectToJson(this);
}

@JsonSerializable()
class IssueType {
  String name;

  IssueType({this.name});

  factory IssueType.fromJson(Map<String, dynamic> json) =>
      _$IssueTypeFromJson(json);
  Map<String, dynamic> toJson() => _$IssueTypeToJson(this);
}

@JsonSerializable()
class CreateIssueResponse {
  String id;
  String key;
  String self;

  CreateIssueResponse({this.id, this.key, this.self});

  factory CreateIssueResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateIssueResponseFromJson(json);
  Map<String, dynamic> toJson() => _$CreateIssueResponseToJson(this);
}
