import 'dart:io';
import 'dart:typed_data';

import 'package:polariz/src/domain/feedback_handler.dart';

class PrintFeedbackHandler extends FeedbackHandler {
  @override
  Future<bool> handleFeedback(String userId, String message,
      Uint8List feedbackScreenshot, List<File> attachments) async {
    print(
        "handling message from user $userId: $message, screenshot? ${feedbackScreenshot != null}, #attachments ${attachments.length}");
    return true;
  }
}
