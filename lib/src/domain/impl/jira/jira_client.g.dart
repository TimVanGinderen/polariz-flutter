// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'jira_client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateIssue _$CreateIssueFromJson(Map<String, dynamic> json) {
  return CreateIssue(
    fields: json['fields'] == null
        ? null
        : Fields.fromJson(json['fields'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CreateIssueToJson(CreateIssue instance) =>
    <String, dynamic>{
      'fields': instance.fields,
    };

Fields _$FieldsFromJson(Map<String, dynamic> json) {
  return Fields(
    summary: json['summary'] as String,
    description: json['description'] as String,
    project: json['project'] == null
        ? null
        : Project.fromJson(json['project'] as Map<String, dynamic>),
    issuetype: json['issuetype'] == null
        ? null
        : IssueType.fromJson(json['issuetype'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$FieldsToJson(Fields instance) => <String, dynamic>{
      'summary': instance.summary,
      'description': instance.description,
      'project': instance.project,
      'issuetype': instance.issuetype,
    };

Project _$ProjectFromJson(Map<String, dynamic> json) {
  return Project(
    key: json['key'] as String,
  );
}

Map<String, dynamic> _$ProjectToJson(Project instance) => <String, dynamic>{
      'key': instance.key,
    };

IssueType _$IssueTypeFromJson(Map<String, dynamic> json) {
  return IssueType(
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$IssueTypeToJson(IssueType instance) => <String, dynamic>{
      'name': instance.name,
    };

CreateIssueResponse _$CreateIssueResponseFromJson(Map<String, dynamic> json) {
  return CreateIssueResponse(
    id: json['id'] as String,
    key: json['key'] as String,
    self: json['self'] as String,
  );
}

Map<String, dynamic> _$CreateIssueResponseToJson(
        CreateIssueResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'self': instance.self,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _JiraClient implements JiraClient {
  _JiraClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
  }

  final Dio _dio;

  String baseUrl;

  @override
  createIssue(body) async {
    ArgumentError.checkNotNull(body, 'body');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body?.toJson() ?? <String, dynamic>{});
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/rest/api/2/issue',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = CreateIssueResponse.fromJson(_result.data);
    return Future.value(value);
  }

  @override
  postAttachment(key, file) async {
    ArgumentError.checkNotNull(key, 'key');
    ArgumentError.checkNotNull(file, 'file');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    _data.files.add(MapEntry(
        'file',
        MultipartFile.fromFileSync(file.path,
            filename: file.path.split(Platform.pathSeparator).last)));
    await _dio.request<void>('/rest/api/2/issue/$key/attachments',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    return Future.value(null);
  }
}
