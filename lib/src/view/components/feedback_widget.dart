import 'package:polariz/src/view/feedback.dart';
import 'package:polariz/src/view/components/controls_column_widget.dart';
import 'package:polariz/src/view/feedback_viewmodel.dart';
import 'package:polariz/src/view/painting/painter.dart';
import 'package:polariz/src/view/translation/translation.dart';
import 'package:polariz/src/view/utils/feedback_functions.dart';
import 'package:polariz/src/view/painting/paint_on_background.dart';
import 'package:polariz/src/view/components/scale_and_clip.dart';
import 'package:polariz/src/view/components/screenshot.dart';
import 'package:flutter/material.dart';

class FeedbackWidget extends StatefulWidget {
  const FeedbackWidget({
    Key key,
    @required this.child,
    @required this.isFeedbackVisible,
    @required this.translation,
    @required this.viewModel,
    this.additionalAttachments,
    this.backgroundColor,
    this.feedback,
    this.drawColors,
  })  : assert(child != null),
        assert(isFeedbackVisible != null),
        assert(translation != null),
        assert(
          drawColors == null || (drawColors != null && drawColors.length > 0),
          'There must be at least one color to draw',
        ),
        super(key: key);

  final bool isFeedbackVisible;
  final OnFeedbackCallback feedback;
  final GetAdditionalAttachments additionalAttachments;
  final Widget child;
  final Color backgroundColor;
  final List<Color> drawColors;
  final FeedbackTranslation translation;
  final FeedbackViewModel viewModel;

  @override
  _FeedbackWidgetState createState() => _FeedbackWidgetState();
}

class _FeedbackWidgetState extends State<FeedbackWidget>
    with SingleTickerProviderStateMixin {
  PainterController painterController;
  ScreenshotController screenshotController = ScreenshotController();
  TextEditingController textEditingController = TextEditingController();

  bool isNavigatingActive = true;
  AnimationController _controller;
  List<Color> drawColors;
  bool keyboardVisible = false;

  PainterController create() {
    final PainterController controller = PainterController();
    controller.thickness = 5.0;
    controller.drawColor = drawColors[0];
    return controller;
  }

  @override
  void initState() {
    super.initState();

    drawColors = widget.drawColors ??
        [
          Colors.red,
          Colors.green,
          Colors.blue,
          Colors.yellow,
        ];

    painterController = create();

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    textEditingController.addListener(() {
      widget.viewModel.updateMessage(textEditingController.text);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  void didUpdateWidget(FeedbackWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.isFeedbackVisible != widget.isFeedbackVisible &&
        oldWidget.isFeedbackVisible == false) {
      // Feedback is now visible,
      // start animation to show it.
      _controller.forward();
    }

    if (oldWidget.isFeedbackVisible != widget.isFeedbackVisible &&
        oldWidget.isFeedbackVisible == true) {
      // Feedback is no longer visible,
      // reverse animation to hide it.
      _controller.reverse();
      painterController.clear();
      _controller.addListener(() {
        if (_controller.status == AnimationStatus.completed) {
          textEditingController.text = "";
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final scaleAnimation = Tween<double>(begin: 1, end: 0.65)
        .chain(CurveTween(curve: Curves.easeInSine))
        .animate(_controller);

    final animation = Tween<double>(begin: 0, end: 1)
        .chain(CurveTween(curve: Curves.easeInSine))
        .animate(_controller);

    final controlsHorizontalAlignment = Tween<double>(begin: 1.4, end: .95)
        .chain(CurveTween(curve: Curves.easeInSine))
        .animate(_controller);

    final inputVerticalAlignment = Tween<double>(begin: -1.9, end: -0.9)
        .chain(CurveTween(curve: Curves.easeInSine))
        .animate(_controller);

    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return Localizations(
          locale: const Locale('en', 'US'),
          delegates: <LocalizationsDelegate<dynamic>>[
            DefaultWidgetsLocalizations.delegate,
            DefaultMaterialLocalizations.delegate,
          ],
          child: Container(
            color: widget.backgroundColor ?? Colors.white,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Align(
                  alignment: Alignment(
                    0,
                    inputVerticalAlignment.value,
                  ),
                  child: Container(
                      height: 200,
                      width: double.infinity,
                      padding: EdgeInsets.all(16.0),
                      child: Card(
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          elevation: 8,
                          child: buildFeedback())),
                ),
                ScaleAndClip(
                  scale: scaleAnimation.value,
                  alignmentProgress: animation.value,
                  child: Screenshot(
                    controller: screenshotController,
                    child: PaintOnBackground(
                      controller: painterController,
                      isPaintingActive:
                          !isNavigatingActive && widget.isFeedbackVisible,
                      child: widget.child,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment(
                    controlsHorizontalAlignment.value,
                    0.5,
                  ),
                  child: ControlsColumn(
                    mode: isNavigatingActive
                        ? ControlMode.navigate
                        : ControlMode.draw,
                    activeColor: painterController.drawColor,
                    translation: widget.translation,
                    colors: drawColors,
                    onColorChanged: (color) {
                      setState(() {
                        painterController.drawColor = color;
                      });
                      _hideKeyboard(context);
                    },
                    onUndo: () {
                      painterController.undo();
                      _hideKeyboard(context);
                    },
                    onClearDrawing: () {
                      painterController.clear();
                      _hideKeyboard(context);
                    },
                    onControlModeChanged: (mode) {
                      setState(() {
                        isNavigatingActive = mode == ControlMode.navigate;
                        _hideKeyboard(context);
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget buildFeedback() {
    final FeedbackData feedbackData = PolarizFeedback.of(context);
    if (feedbackData.isLoading)
      return Center(
        child: CircularProgressIndicator(),
      );
    if (feedbackData.isSuccess)
      return Icon(
        Icons.check,
        size: 48,
        color: Colors.green,
      );
    if (feedbackData.isError)
      return Icon(
        Icons.close,
        size: 48,
        color: Colors.red,
      );
    if (feedbackData.isPending)
      return buildInput();
    else
      return Container();
  }

  Widget buildInput() {
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                widget.translation.feedbackDescriptionText,
                maxLines: 2,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
              ),
              TextField(
                maxLines: 4,
                minLines: 4,
                controller: textEditingController,
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              IconButton(
                color: widget.backgroundColor,
                icon: Icon(Icons.send),
                onPressed: () {
                  _hideKeyboard(context);
                  sendFeedback(context);
                },
              ),
              IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  _hideKeyboard(context);
                  PolarizFeedback.of(context).hide();
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<void> sendFeedback(BuildContext context) async {
    _hideKeyboard(context);

    // Wait for the keyboard to be closed, and then proceed
    // to take a screenshot
    await Future.delayed(const Duration(milliseconds: 200), () async {
      final screenshot = await screenshotController.capture(pixelRatio: 3);
      widget.viewModel.handleFeedback(screenshot);
    });
  }

  void _hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }
}
