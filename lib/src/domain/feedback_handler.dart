import 'dart:io';
import 'dart:typed_data';

abstract class FeedbackHandler {
  Future<bool> handleFeedback(String userId, String feedback,
      Uint8List feedbackScreenshot, List<File> attachments);
}
